import keras
import matplotlib.pyplot as plt
import numpy as np

import src.algos.constants.constants as constants
from src.algos import metrics


# predictions
def predict(data, algorithm_type, algorithm_model, trained_models_root_path, topology_component):
    print("Starting prediction using the model %s." % algorithm_type)

    trained_models_path = "%s/%s/%s" % (trained_models_root_path, algorithm_type, topology_component)

    with keras.backend.get_session().graph.as_default():
        algorithm_model.load_model(trained_models_path)
        print('Done loading the model.')

        predictions, ys = algorithm_model.predict(data)
        print('Done predicting the data.')

    return predictions, ys


def predict_flexibility(data, algorithm_type, algorithm_model, topology_component, flexibility_type, data_scenario):
    trained_models_root_path = constants.get_flexibility_trained_models_path(data_scenario, flexibility_type)
    trained_models_path = "%s/%s/%s" % (trained_models_root_path, algorithm_type, topology_component)

    with keras.backend.get_session().graph.as_default():
        algorithm_model.load_model(trained_models_path)
        print('Done loading the model.')

        predictions, ys = algorithm_model.predict(data)
        print('Done predicting the data.')

    return predictions, ys


# plots
def plot_prediction(predictions, test_data, prediction_type, data_scenario, topology_component, algorithm_type,
                    flexibility_type=None):
    no_models = len(predictions)
    all_predictions = list()
    all_expected = list()

    mapes = list()
    maes = list()
    for i in range(no_models):
        prediction_per_model = predictions[i]
        expected_data_per_model = test_data[i]
        no_samples = prediction_per_model.shape[0]

        for j in range(no_samples):
            prediction = prediction_per_model[j, :]
            expected_data = expected_data_per_model[j]

            title = "%s: %s - %s - %s - model: %d/%d, sample: %d/%d" \
                    % (data_scenario, algorithm_type, prediction_type, topology_component, i + 1, no_models, j + 1,
                       no_samples)

            if flexibility_type is None:
                saving_path = "%s/%s/%s/%s/result_%s_%s_model_%d-outOf-%d_sample_%d-outOf-%d_%s.png" % \
                              (constants.get_results_path(data_scenario), prediction_type, topology_component,
                               algorithm_type, data_scenario, algorithm_type, i + 1, no_models, j + 1, no_samples,
                               topology_component)
            else:
                root_path = constants.get_flexibility_results_path(data_scenario)
                saving_path = "%s/%s/results_%s_sample_%d-outOf-%d_%s.png" % \
                              (root_path, flexibility_type, algorithm_type, j + 1, no_samples,
                               topology_component)

            plt.figure()
            plt.title(title)

            y_low, y_high = get_plot_limits(prediction_type, flexibility_type, data_scenario)
            plt.ylim(y_low, y_high)

            all_predictions.append(prediction)
            all_expected.append(expected_data)

            if prediction_type == constants.NEAR_REAL_TIME:
                plt.plot(prediction, 'b+')
                plt.plot(expected_data, 'r+')
            else:
                plt.plot(prediction)
                plt.plot(expected_data)

            plt.legend(['predicted', 'expected'], loc='upper left')

            if flexibility_type is None:
                mape = metrics.Metrics.mean_absolute_percentage_error(expected_data, prediction)
                mae = metrics.Metrics.mean_absolute_error(expected_data, prediction)

                mapes.append(mape)
                maes.append(mae)

                plt.figtext(.6, .8, "MAPE = %.5f\nMAE = %.5f" % (mape, mae))
            else:
                rmse = metrics.Metrics.root_mean_square_error(expected_data, prediction)
                plt.figtext(.6, .8, "RMSE = %.5f" % rmse)
            plt.savefig(saving_path)
            plt.draw()

            plt.clf()
            plt.cla()
            plt.close()

    if flexibility_type is None:
        print('\n')
        print("All samples average prediction MAPE for %s: %0.5f" % (algorithm_type, np.average(mapes)))
        print("All samples average prediction MAE for %s: %0.5f" % (algorithm_type, np.average(maes)))

        print("Best MAPE for %s: %0.5f" % (algorithm_type, np.min(mapes)))
        print("Best MAE for %s: %0.5f" % (algorithm_type, np.min(maes)))
        print('\n')


def get_plot_limits(prediction_type, flexibility_type, data_scenario):
    if flexibility_type is None:
        return 0.0, 1500.0

    elif prediction_type == constants.DAYAHEAD:
        # Scenario 0 - Paper
        if data_scenario == constants.DATA_SCENARIO[0]:
            if flexibility_type == constants.FLEXIBILITY_TYPE_UPPER:
                return 0.0, 1000.0

            elif flexibility_type == constants.FLEXIBILITY_TYPE_LOWER:
                return -1000.0, 0.0

        # Scenario 1 - Poznan
        elif data_scenario == constants.DATA_SCENARIO[1]:
            if flexibility_type == constants.FLEXIBILITY_TYPE_UPPER:
                return -500.0, 500.0
            elif flexibility_type == constants.FLEXIBILITY_TYPE_LOWER:
                return -500.0, 500.0

    return None


def plot_loss(histories, algorithm_type, prediction_type, data_scenario, topology_component, flexibility_type=None):
    histories_max_index = len(histories)

    for i, history in enumerate(histories):
        title = "%s: %s - %s - %s - Model %d/%d" % \
                (data_scenario, topology_component, algorithm_type, prediction_type, i + 1, histories_max_index)

        if flexibility_type is None:
            saving_path = "%s/%s/%s/loss_%s_sample_%d-outOf-%d.png" % \
                          (constants.get_loss_path(data_scenario), prediction_type, topology_component,
                           algorithm_type, i + 1, histories_max_index)
        else:
            root_path = constants.get_flexibility_loss_path(data_scenario)
            saving_path = "%s/%s/loss_%s_sample_%d-outOf-%d.png" % \
                          (root_path, flexibility_type, algorithm_type, i + 1, histories_max_index)

        plt.title(title)
        plt.plot(history.history['loss'], label='train')
        plt.plot(history.history['val_loss'], label='test')
        plt.ylabel('loss')
        plt.xlabel('epoch')
        plt.legend(['train', 'validation'], loc='upper right')
        plt.savefig(saving_path)
        plt.draw()

        plt.clf()
        plt.cla()
        plt.close()


# utils
def extract_energy_values(json_energy_profile):
    energy_values = json_energy_profile['curve']
    energy_values = [energy_values[i]['energyValue'] for i in range(len(energy_values))]
    energy_values = np.array(energy_values).reshape(1, -1)

    return energy_values
