# Paths
TRAINING_DATA = "../src/%s/training_data/%s"
TEST_DATA = "../src/%s/test_data/%s"
TRAINED_MODELS = "../src/%s/trained_models/%s"
# Results
RESULTS_PATH = '../src/%s/results'
LOSS_PATH = '../src/%s/loss'


# Functions use DATA_SCENARIO and PREDICTION_TYPE (DA/ID/NRT) constants from this file to construct the requested path.
def get_training_data_path(data_scenario, prediction_type):
    return TRAINING_DATA % (data_scenario, prediction_type)


def get_test_data_path(data_scenario, prediction_type):
    return TEST_DATA % (data_scenario, prediction_type)


def get_trained_models_path(data_scenario, prediction_type):
    return TRAINED_MODELS % (data_scenario, prediction_type)


def get_results_path(data_scenario):
    return RESULTS_PATH % data_scenario


def get_loss_path(data_scenario):
    return LOSS_PATH % data_scenario
# #################################################################################


# ### Flexibility
# Paths
FLEXIBILITY_TRAINING_DATA = '../src/%s/flexibility/training_data/%s'
FLEXIBILITY_TEST_DATA = '../src/%s/flexibility/test_data/%s'
FLEXIBILITY_TRAINED_MODELS = '../src/%s/flexibility/trained_models/%s'
# Results
FLEXIBILITY_RESULTS_PATH = '../src/%s/flexibility/results'
FLEXIBILITY_LOSS_PATH = '../src/%s/flexibility/loss'


# Functions use DATA_SCENARIO & FLEXIBILITY_TYPE (Upper/Lower) constants from this file to construct the requested path.
def get_flexibility_results_path(data_scenario):
    return FLEXIBILITY_RESULTS_PATH % data_scenario


def get_flexibility_loss_path(data_scenario):
    return FLEXIBILITY_LOSS_PATH % data_scenario


def get_flexibility_training_data_path(data_scenario, flexibility_type):
    return FLEXIBILITY_TRAINING_DATA % (data_scenario, flexibility_type)


def get_flexibility_test_data_path(data_scenario, flexibility_type):
    return FLEXIBILITY_TEST_DATA % (data_scenario, flexibility_type)


def get_flexibility_trained_models_path(data_scenario, flexibility_type):
    return FLEXIBILITY_TRAINED_MODELS % (data_scenario, flexibility_type)


# ### Business logic
# Prediction type
DAYAHEAD = 'dayahead'
INTRADAY = 'intraday'
NEAR_REAL_TIME = 'near_real_time'

# Data source scenario
DATA_SCENARIO = [
    'paper',
    'poznan'
]

# Algorithm type
ALGORITHM_TYPE_MLP = 'mlp'

# DataCenter components
SERVER_COMPONENT = 'SERVER_ROOM'
COOLING_SYSTEM_COMPONENT = 'COOLING_SYSTEM'

# Flexibility type
FLEXIBILITY_TYPE_UPPER = 'UPPER'
FLEXIBILITY_TYPE_LOWER = 'LOWER'

SCENARIO_TRAIN = 'train'
SCENARIO_TEST = 'test'

