from collections import namedtuple

# Parameters contract for algorithms
MLP_PARAMETERS_NAME = ['no_inputs', 'no_outputs', 'no_neurons', 'no_hidden_layers', 'batch_size', 'no_epochs']

# dayahead
MLP_DAYAHEAD = namedtuple('MLP_DAYAHEAD', MLP_PARAMETERS_NAME)
MLP_DAYAHEAD_SERVER = [
    # model 0
    MLP_DAYAHEAD(no_inputs=25, no_outputs=24, no_neurons=24, no_hidden_layers=1, batch_size=200, no_epochs=490)
]

MLP_DAYAHEAD_SERVER_POZNAN = [
    # model 0
    MLP_DAYAHEAD(no_inputs=24, no_outputs=24, no_neurons=37, no_hidden_layers=1, batch_size=2, no_epochs=100)
]
# intraday
MLP_INTRADAY = namedtuple('MLP_INTRADAY', MLP_PARAMETERS_NAME)
MLP_INTRADAY_SERVER = [
    # model 0
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=24, no_hidden_layers=1, batch_size=8, no_epochs=200),
    # model 1
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=20, no_hidden_layers=1, batch_size=8, no_epochs=100),
    # model 2
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=20, no_hidden_layers=1, batch_size=8, no_epochs=200),
    # model 3
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=20, no_hidden_layers=1, batch_size=8, no_epochs=100),
    # model 4
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=12, no_hidden_layers=1, batch_size=20, no_epochs=250),
    # model 5
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=20, no_hidden_layers=1, batch_size=8, no_epochs=200)
]

MLP_INTRADAY_SERVER_POZNAN = [
    # model 0
    MLP_INTRADAY(no_inputs=8, no_outputs=8, no_neurons=28, no_hidden_layers=1, batch_size=16, no_epochs=200),
]

# near real time
MLP_NEAR_REAL_TIME = namedtuple('MLP_NEAR_REAL_TIME', MLP_PARAMETERS_NAME)
MLP_NEAR_REAL_TIME_SERVER = [
    # model 0
    MLP_INTRADAY(no_inputs=24, no_outputs=1, no_neurons=24, no_hidden_layers=1, batch_size=300, no_epochs=120),
]

MLP_NEAR_REAL_TIME_SERVER_POZNAN = [
    # model 0
    MLP_INTRADAY(no_inputs=24, no_outputs=1, no_neurons=37, no_hidden_layers=1, batch_size=2, no_epochs=100),
]
# Flexibility
MLP_DAYAHEAD_FLEXIBILITY = namedtuple('MLP_DAYAHEAD_FLEXIBILITY', MLP_PARAMETERS_NAME)
MLP_DAYAHEAD_UPPER_BOUNDS = [
    # model 0
    MLP_DAYAHEAD_FLEXIBILITY(no_inputs=24, no_outputs=24, no_neurons=40, no_hidden_layers=1, batch_size=300, no_epochs=600)
]
MLP_DAYAHEAD_LOWER_BOUNDS = [
    # model 0
    MLP_DAYAHEAD_FLEXIBILITY(no_inputs=24, no_outputs=24, no_neurons=32, no_hidden_layers=1, batch_size=300, no_epochs=700)
]

MLP_DAYAHEAD_UPPER_BOUNDS_POZNAN = [
    # model 0
    MLP_DAYAHEAD_FLEXIBILITY(no_inputs=24, no_outputs=24, no_neurons=40, no_hidden_layers=1, batch_size=300, no_epochs=600)
]
MLP_DAYAHEAD_LOWER_BOUNDS_POZNAN = [
    # model 0
    MLP_DAYAHEAD_FLEXIBILITY(no_inputs=24, no_outputs=24, no_neurons=32, no_hidden_layers=1, batch_size=300, no_epochs=700)
]
