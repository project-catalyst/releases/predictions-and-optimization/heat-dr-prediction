import numpy as np
import pandas as pd
import os
import json
from flask import Flask, request, jsonify, Response
from flask_cors import CORS
import tensorflow as tf
import mlp

app = Flask(__name__)
CORS(app)
graph = tf.get_default_graph()


@app.route("/compute/<rack1_load>/<rack2_load>/<air_t>/<air_f>/<init_temp>", methods=['GET'])
def compute(rack1_load, rack2_load, air_t, air_f, init_temp):
    global model, scaler
    ret = ['' for _ in range(14)]

    inp = mlp.scale_input(scaler, np.array([init_temp, air_t, air_f, rack1_load, rack2_load]))

    predict = list()
    for col in range(14):
        p = np.array([[0]])
        for i in range(60):
            with graph.as_default():
                pred = model[i].predict(inp)
            p2 = pred[:, col::14].reshape(-1, 1)
            p = np.vstack([p, p2])
        p = p[1:]
        predict.append(mlp.invert_scale(scaler, p))

    temp = np.array([predict[i][:100:10] for i in range(len(predict))]).reshape(1, -1)
    ret = np.str(temp).replace('\n', '').strip("[]'").replace('  ', ' ').replace(' ', ',').replace(',,', ',')

    # return jsonify(rounded)
    return jsonify(str(ret).strip("[]'"))


if __name__ == "__main__":
    global model, scaler
    dataset = pd.read_csv('data_fixed_time.csv', sep=',', header=-1, low_memory=False)
    n = int(dataset.shape[0] * 0.8)
    train = dataset[:n]
    test = dataset[n:]
    scaler, train, test = mlp.scale(train, test)
    model = mlp.load_models(60)
    app.run(host='0.0.0.0', port=9091, debug=False)
