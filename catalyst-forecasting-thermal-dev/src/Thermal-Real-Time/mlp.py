from pandas import read_csv
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
from keras import Sequential
from keras.layers import Dense
from keras.models import load_model, model_from_json
from matplotlib import pyplot as plt
from matplotlib import ticker as plticker
import numpy as np
from pandas import DataFrame

dataset = read_csv('data_fixed_time.csv', sep=',', header=-1, low_memory=False)

field_names = ['elapsed_time',
               't_room_initial'
               't_air_input',
               'air_flow',
               'heat_generation_rate_server_1',
               'heat_generation_rate_server_2',

               'previous_inlet_probe_1_rack_1',
               'previous_inlet_probe_2_rack_1',
               'previous_inlet_probe_3_rack_1',
               'previous_inlet_probe_4_rack_1',
               'previous_inlet_probe_1_rack_2',
               'previous_inlet_probe_2_rack_2',
               'previous_inlet_probe_3_rack_2',
               'previous_inlet_probe_4_rack_2',
               'previous_outlet_probe_1_rack_1',
               'previous_outlet_probe_1_rack_2',
               'previous_outlet_probe_1_room',
               'previous_outlet_probe_2_rack_1',
               'previous_outlet_probe_2_rack_2',
               'previous_outlet_probe_2_room',

               'current_inlet_probe_1_rack_1',
               'current_inlet_probe_2_rack_1',
               'current_inlet_probe_3_rack_1',
               'current_inlet_probe_4_rack_1',
               'current_inlet_probe_1_rack_2',
               'current_inlet_probe_2_rack_2',
               'current_inlet_probe_3_rack_2',
               'current_inlet_probe_4_rack_2',
               'current_outlet_probe_1_rack_1',
               'current_outlet_probe_1_rack_2',
               'current_outlet_probe_1_room',
               'current_outlet_probe_2_rack_1',
               'current_outlet_probe_2_rack_2',
               'current_outlet_probe_2_room',
               ]

names = ['inlet_probe_1_rack_1',
         'inlet_probe_2_rack_1',
         'inlet_probe_3_rack_1',
         'inlet_probe_4_rack_1',
         'inlet_probe_1_rack_2',
         'inlet_probe_2_rack_2',
         'inlet_probe_3_rack_2',
         'inlet_probe_4_rack_2',
         'outlet_probe_1_rack_1',
         'outlet_probe_1_rack_2',
         'outlet_probe_1_room',
         'outlet_probe_2_room',
         'outlet_probe_3_room',
         'outlet_probe_4_room'
         ]

nr_outputs = 14
time_len = 600
nr_steps = 10


def scale(train, test):
    scaler = MinMaxScaler(feature_range=(-1, 1))
    scaler = scaler.fit(train)

    # scaler.data_min_[1] = 10 + 273.15
    # scaler.data_max_[1] = 40 + 273.15
    #
    # scaler.data_min_[2] = 10 + 273.15
    # scaler.data_max_[2] = 40 + 273.15
    #
    # scaler.data_min_[3] = 0
    # scaler.data_max_[3] = 5

    temp_max = max(scaler.data_max_[6:])
    temp_min = min(scaler.data_min_[6:])

    for i in range(6, len(scaler.data_max_)):
        scaler.data_max_[i] = temp_max
        scaler.data_min_[i] = temp_min

    temp1 = np.array([scaler.data_min_[i] for i in range(len(scaler.data_max_))])
    temp2 = np.array([scaler.data_max_[i] for i in range(len(scaler.data_min_))])
    temp = np.vstack((temp1, temp2, train))

    scaler = scaler.fit(temp)

    train_scaled = scaler.transform(train)

    test_scaled = scaler.transform(test)

    return scaler, train_scaled, test_scaled


def invert_scale(scaler, inp):
    dummy = np.hstack((np.zeros((inp.shape[0], 19)), inp.reshape(-1, 1)))
    dummy = scaler.inverse_transform(dummy)
    return dummy[:, -1].reshape(1, -1).flatten()


def define_model(input_dim, output_size):
    model = Sequential()
    model.add(Dense(50, activation='relu', input_dim=input_dim))
    model.add(Dense(20, kernel_initializer='he_uniform', activation='relu'))
    model.add(Dense(output_size))
    model.compile(loss='mean_squared_error', optimizer='adam')
    return model


def fit_model(train, batch_size, nb_epoch, output_size, step_ahead, steps):
    # if step_ahead == 0:
    #     x, y = train[:, 1:-output_size], train[:, -output_size:]
    # else:
    #     inp = np.array(train)[:, 1:-output_size]
    #     out = np.array(train[:, -output_size:])
    #
    #     x = np.empty(inp.shape[1])
    #     y = np.empty(out.shape[1])
    #     for i in range(inp.shape[0]):
    #         if (i % 30) < 30 - step_ahead:
    #             x = np.vstack((x, inp[i].reshape(1, -1)))
    #         if (i % 30) >= step_ahead:
    #             y = np.vstack((y, out[i].reshape(1, -1)))
    #     x = x[1:]
    #     y = y[1:]
    train_array = np.array(train)
    x = train_array[::time_len, 1:-output_size]
    y = train_array[step_ahead::time_len, -output_size:]
    y1 = np.hstack([train_array[step_ahead + i::time_len, -output_size:] for i in range(steps)])

    model = define_model(x.shape[1], y1.shape[1])
    model.fit(x, y1, epochs=nb_epoch, batch_size=batch_size, shuffle=True, verbose=1)
    return model


def save_models(model):
    for i in range(len(model)):
        # model[i].save("models/mlp/new2/model{nr}.h5".format(nr=i))
        model[i].save_weights('models/mlp/new2split/model{nr}.h5'.format(nr=i))
        model_json = model[i].to_json()
        with(open('models/mlp/new2split/model{nr}.json'.format(nr=i), 'w')) as json_file:
            json_file.write(model_json)
        json_file.close()


def load_models(n):
    model = list()
    for i in range(int(n)):
        json_file = open('models/mlp/new2split/model{nr}.json'.format(nr=i))
        loaded_json_model = json_file.read()
        json_file.close()
        model1 = model_from_json(loaded_json_model)
        model1.load_weights('models/mlp/new2split/model{nr}.h5'.format(nr=i))
        # model.append(load_model("models/mlp/new2/model{nr}.h5".format(nr=i)))
        model.append(model1)
        print(i)
    return model


def scale_input(scaler, inp):
    dummy = np.hstack((np.zeros(1), inp, np.zeros(nr_outputs)))
    dummy = dummy.reshape(1, -1)
    dummy = scaler.transform(dummy).flatten()
    return dummy[1:-nr_outputs].reshape(1, -1)


def forecast(model, x, step, column, scaler, scaled=False):
    if not scaled:
        scale_input(scaler, x)

    y = model[step].predict(x)[0][column]
    y = invert_scale(scaler, y)

    return y


def simulation(scaler, model):
    t_room_init = 18 + 273.15
    t_air_init = 18 + 273.15

    air_flow = 1
    heat_server1 = 4000
    heat_server2 = 4000

    plots = [list() for _ in range(4)]

    n = int(time_len / nr_steps)

    for t in range(0, 8400, time_len):  # up to 2h20m
        inp = scale_input(scaler, np.array([t_room_init, t_air_init, air_flow, heat_server1, heat_server2]))

        predict = list()
        for col in range(10, 14):  # take the last 4 columns
            p = np.array([[0]])
            for i in range(n):
                pred = model[i].predict(inp)
                p2 = pred[:, col::nr_outputs].reshape(-1, 1)
                p = np.vstack([p, p2])
            p = p[1:]
            predict.append(invert_scale(scaler, p))

        new_temp = 0
        for i in range(4):
            new_temp += predict[i][time_len - 1] + 1
        new_temp /= 4

        plots[0].append(predict[0])
        plots[1].append(predict[1])
        plots[2].append(predict[2])
        plots[3].append(predict[3])

        t_room_init = new_temp

    plt.figure(figsize=(100, 10))
    ax = plt.subplot(1, 1, 1)
    ax.xaxis.set_major_locator(plticker.MultipleLocator(base=60))
    ax.xaxis.set_minor_locator(plticker.MultipleLocator(base=10))
    ax.yaxis.set_major_locator(plticker.MultipleLocator(base=1))
    ax.yaxis.set_minor_locator(plticker.MultipleLocator(base=0.5))
    plt.plot(np.array(plots[0]).flatten())
    plt.plot(np.array(plots[1]).flatten())
    plt.plot(np.array(plots[2]).flatten())
    plt.plot(np.array(plots[3]).flatten())
    plt.savefig('plots/test', dpi=100)
    plt.clf()
    plt.clf()


def main():
    n = int(dataset.shape[0] * 0.8)
    train = dataset[:n]
    test = dataset[n:]
    scaler, train, test = scale(train, test)

    n = int(time_len / nr_steps)
    # model = model_load()
    # model = [fit_model(train, 10, 50, nr_outputs, step, nr_steps) for step in range(0, time_len, nr_steps)]

    model = load_models(n)

    predicts = None
    for col in range(nr_outputs):
        input = np.array(test[4200, 1:-nr_outputs]).reshape(1, -1)

        actual = test[4200:4200 + time_len, -(nr_outputs - col)]
        pred = np.array([[0]])
        for i in range(n):
            p = model[i].predict(input)
            p2 = p[:, col::nr_outputs].reshape(-1, 1)
            pred = np.vstack([pred, p2])
            # pred.append(model[i].predict(input)[0][col])
        pred = pred[1:]

        actual = invert_scale(scaler, actual)
        pred = invert_scale(scaler, np.array(pred))

        if predicts is None:
            predicts = pred.reshape(-1, 1)
        else:
            predicts = np.hstack((predicts, pred.reshape(-1, 1)))

    predicts = DataFrame(predicts)
    predicts.to_csv("predicted_data_marcel.csv")
    exit(0)

    # save_models(model)
    # exit()

    # simulation(scaler, model)
    #
    # exit()

    plt.figure(figsize=(10, 100))

    errors = [[list() for _ in range(nr_outputs)] for _ in range(2)]

    for k in range(0, test.shape[0], time_len):
        for col in range(nr_outputs):
            input = np.array(test[k, 1:-nr_outputs]).reshape(1, -1)

            actual = test[k:k + time_len, -(nr_outputs - col)]
            pred = np.array([[0]])
            for i in range(n):
                p = model[i].predict(input)
                p2 = p[:, col::nr_outputs].reshape(-1, 1)
                pred = np.vstack([pred, p2])
                # pred.append(model[i].predict(input)[0][col])
            pred = pred[1:]

            actual = invert_scale(scaler, actual)
            pred = invert_scale(scaler, np.array(pred))

            errors[0][col].append(actual)
            errors[1][col].append(pred)

            error = mean_squared_error(actual, pred)

            ax = plt.subplot(nr_outputs, 1, (col + 1))
            ax.xaxis.set_major_locator(plticker.MultipleLocator(base=30))
            ax.xaxis.set_minor_locator(plticker.MultipleLocator(base=10))
            ax.yaxis.set_major_locator(plticker.MultipleLocator(base=1))
            ax.yaxis.set_minor_locator(plticker.MultipleLocator(base=0.5))
            plt.xlabel('Time (s)')
            plt.ylabel('Temperature (K)')
            plt.ylim((280, 320))
            plt.title(names[col] + (" (MSE=%.5f)" % error))
            plt.grid(True)
            plt.plot(actual, 'b')
            plt.plot(pred, 'r')
            plt.legend(('Actual value', 'Predicted value'))
        avg_error = 0
        for i in range(nr_outputs):
            avg_error += mean_squared_error(errors[0][i][int(k / time_len)], errors[1][i][int(k / time_len)])
        avg_error /= nr_outputs
        plt.figtext(0.4, 0.0005, 'Average error: {error}'.format(error=avg_error))
        plt.tight_layout()
        plt.savefig("plots/mlp/new2/fig{nr}".format(nr=int(k / time_len)), dpi=100)
        plt.clf()

    for i in range(nr_outputs):
        print("{names}: {error}".format(names=names[i], error=mean_squared_error(errors[0][i], errors[1][i])))


if __name__ == "__main__":
    main()
