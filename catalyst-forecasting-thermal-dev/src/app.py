import pandas as pd
import tensorflow as tf
from flask import Flask
from keras.backend.tensorflow_backend import set_session
import src.algos.mappings as mapping
from src.algos.constants import constants
from src.algos.mlp import mlp
from src.algos import utils
from src.endpoints.dayahead.dayahead_predict import dayahead_predict
from src.endpoints.intraday.intraday_predict import intraday_predict
from src.endpoints.near_real_time.near_real_time_predict import near_real_time_predict

config1 = tf.ConfigProto()
config1.gpu_options.allow_growth = True  # dynamically grow the memory used on the GPU
config1.log_device_placement = True  # to log device placement (on which device the operation ran)
sess = tf.Session(config=config1)
set_session(sess)  # set this TensorFlow session as the default session for Keras

app = Flask(__name__)

# register endpoints
app.register_blueprint(dayahead_predict, url_prefix='/dayahead')
app.register_blueprint(intraday_predict, url_prefix='/intraday')
app.register_blueprint(near_real_time_predict, url_prefix='/near_real_time')


def train_model(algorithm_type, prediction_type, data_scenario, algorithm_model, train_data_path,
                trained_models_root_path, topology_component, flexibility_type=None):
    print("Starting training the model %s." % algorithm_type)

    train_data_path = "%s_%s.csv" % (train_data_path, topology_component)
    train_data = pd.read_csv(train_data_path, sep=',').values

    history = algorithm_model.train_model(train_data, prediction_type, data_scenario)
    print('Done training model, saving to disk...')

    utils.plot_loss(history, algorithm_type, prediction_type, data_scenario, topology_component, flexibility_type)

    trained_models_path = "%s/%s" % (trained_models_root_path, algorithm_type)
    algorithm_model.save_model(trained_models_path, topology_component)
    print('Saved model to disk. Finished training.')


def predict(algorithm_type, algorithm_model, data_path, trained_models_root_path, prediction_type, data_scenario,
            topology_component, flexibility_type=None):
    print('Starting prediction using the model %s. ' % algorithm_type)

    predict_data_path = "%s_%s.csv" % (data_path, topology_component)
    data = pd.read_csv(predict_data_path, sep=',')

    algorithm_model.no_models = mapping.get_no_models_for_algorithm(algorithm_type, prediction_type, data_scenario)
    algorithm_model.prediction_type = prediction_type

    trained_models_path = "%s/%s/%s" % (trained_models_root_path, algorithm_type, topology_component)
    algorithm_model.load_model(trained_models_path)
    print("Done loading the model %s." % trained_models_path)

    predictions, expected_data = algorithm_model.predict(data)
    print('Done predicting the data.')

    utils.plot_prediction(predictions, expected_data, prediction_type, data_scenario, topology_component,
                          algorithm_type, flexibility_type)
    print("Done plotting the predictions results of %s." % algorithm_type)


def call_flexibility_model(algorithm_type, prediction_type, data_scenario, topology_component, flexibility_type):
    training_data_root_path, trained_models_root_path, test_data_root_path = mapping.get_flexibility_paths(
        flexibility_type, data_scenario)
    model_config = mapping.get_config_for_flexibility_algorithm(prediction_type, flexibility_type, data_scenario)
    algorithm_model = None
    if algorithm_type == constants.ALGORITHM_TYPE_MLP:
        algorithm_model = mlp.MLP(model_config)

    train_model(algorithm_type=algorithm_type,
                prediction_type=prediction_type,
                data_scenario=data_scenario,
                algorithm_model=algorithm_model,
                train_data_path="%s/%s" % (training_data_root_path, constants.SCENARIO_TRAIN),
                trained_models_root_path=trained_models_root_path,
                topology_component=topology_component,
                flexibility_type=flexibility_type)

    predict(algorithm_type=algorithm_type,
            algorithm_model=algorithm_model,
            data_path="%s/%s" % (test_data_root_path, constants.SCENARIO_TEST),
            trained_models_root_path=trained_models_root_path,
            prediction_type=prediction_type,
            topology_component=topology_component,
            data_scenario=data_scenario,
            flexibility_type=flexibility_type)


def call_algorithm_model(algorithm_type, prediction_type, data_scenario, topology_component):
    training_data_root_path, trained_models_root_path, test_data_root_path = mapping.get_paths(prediction_type,
                                                                                               data_scenario)
    model_config = mapping.get_model_config(algorithm_type, prediction_type, data_scenario)
    algorithm_model = None
    if algorithm_type == constants.ALGORITHM_TYPE_MLP:
        algorithm_model = mlp.MLP(model_config)

    train_model(algorithm_type=algorithm_type,
                prediction_type=prediction_type,
                data_scenario=data_scenario,
                algorithm_model=algorithm_model,
                train_data_path="%s/%s" % (training_data_root_path, constants.SCENARIO_TRAIN),
                trained_models_root_path=trained_models_root_path,
                topology_component=topology_component)

    predict(algorithm_type=algorithm_type,
            algorithm_model=algorithm_model,
            data_path="%s/%s" % (test_data_root_path, constants.SCENARIO_TEST),
            trained_models_root_path=trained_models_root_path,
            prediction_type=prediction_type,
            data_scenario=data_scenario,
            topology_component=topology_component)


if __name__ == '__main__':
    print('Starting the Electrical prediction application...')

    # ###
    # ###
    # ### Scenario 0 - Paper

    # Server Room
    # MLP
    # call_algorithm_model(constants.ALGORITHM_TYPE_MLP, constants.DAYAHEAD, constants.DATA_SCENARIO[0], constants.SERVER_COMPONENT)
    # call_algorithm_model(constants.ALGORITHM_TYPE_MLP, constants.INTRADAY, constants.DATA_SCENARIO[0], constants.SERVER_COMPONENT)
    # call_algorithm_model(constants.ALGORITHM_TYPE_MLP, constants.NEAR_REAL_TIME, constants.DATA_SCENARIO[0], constants.SERVER_COMPONENT)

    # FLEXIBILITY
    # call_flexibility_model(constants.ALGORITHM_TYPE_MLP, constants.DAYAHEAD, constants.DATA_SCENARIO[0], constants.SERVER_COMPONENT, constants.FLEXIBILITY_TYPE_UPPER)
    # call_flexibility_model(constants.ALGORITHM_TYPE_MLP, constants.DAYAHEAD, constants.DATA_SCENARIO[0], constants.SERVER_COMPONENT, constants.FLEXIBILITY_TYPE_LOWER)

    # ###
    # ###
    # ### Scenario 1 - Poznan

    # Server Room
    # MLP
    # call_algorithm_model(constants.ALGORITHM_TYPE_MLP, constants.DAYAHEAD, constants.DATA_SCENARIO[1], constants.SERVER_COMPONENT)
    # call_algorithm_model(constants.ALGORITHM_TYPE_MLP, constants.INTRADAY, constants.DATA_SCENARIO[1], constants.SERVER_COMPONENT)
    # call_algorithm_model(constants.ALGORITHM_TYPE_MLP, constants.NEAR_REAL_TIME, constants.DATA_SCENARIO[1], constants.SERVER_COMPONENT)

    # FLEXIBILITY
    # call_flexibility_model(constants.ALGORITHM_TYPE_MLP, constants.DAYAHEAD, constants.DATA_SCENARIO[1], constants.SERVER_COMPONENT, constants.FLEXIBILITY_TYPE_UPPER)
    # call_flexibility_model(constants.ALGORITHM_TYPE_MLP, constants.DAYAHEAD, constants.DATA_SCENARIO[1], constants.SERVER_COMPONENT, constants.FLEXIBILITY_TYPE_LOWER)

    # app.run(debug=False, port=5000)
