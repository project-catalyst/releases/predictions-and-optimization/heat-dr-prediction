import datetime
import json
from flask import Blueprint, request, Response
from src.algos.mappings import get_config_for_algorithms
from src.algos.mlp import mlp
from src.algos.utils import *

intraday_predict = Blueprint('intraday', __name__)


@intraday_predict.route('/predict/<algorithm_type>/<data_scenario>/<topology_component>/<timestamp>', methods=['POST'])
def predict_intraday(algorithm_type, data_scenario, topology_component, timestamp):
    energy_values = extract_energy_values(request.get_json()).reshape(1, -1)

    predictions = sample_predict_intraday(energy_values, algorithm_type, data_scenario, topology_component, timestamp)

    return Response(response=json.dumps(predictions), status=200, mimetype='application/json')


def sample_predict_intraday(sample, algorithm_type, data_scenario, topology_component, timestamp):
    # pre-process sample data based on scenario
    data_scenario = data_scenario.lower()
    part_of_day = None
    if data_scenario == constants.DATA_SCENARIO[0]:  # Scenario 0 - Paper
        date_time_obj = datetime.datetime.strptime(timestamp, '%Y-%m-%dT%H:%M')
        part_of_day = date_time_obj.hour // 4
        sample = np.append(sample, 0).reshape(1, -1)
    elif data_scenario == constants.DATA_SCENARIO[1]:  # Scenario 1 - Poznan
        part_of_day = 0
        sample = np.append(sample, 0).reshape(1, -1)

    mlp_config = get_config_for_algorithms(constants.INTRADAY, data_scenario)
    mlp_config = [mlp_config[part_of_day]]
    algorithm_model = mlp.MLP(mlp_config)
    algorithm_model.no_models = 1
    algorithm_model.prediction_type = constants.INTRADAY

    trained_models_root_path = constants.get_trained_models_path(data_scenario, constants.INTRADAY)
    trained_models_path = "%s/%s/%s" % (trained_models_root_path, algorithm_type, topology_component)

    with keras.backend.get_session().graph.as_default():
        algorithm_model.load_single_model(trained_models_path, part_of_day)
        print('Done loading the model.')

        predictions, _ = algorithm_model.predict(sample)
        print('Done predicting the data.')

    predictions = np.ravel(predictions).tolist()

    return predictions
