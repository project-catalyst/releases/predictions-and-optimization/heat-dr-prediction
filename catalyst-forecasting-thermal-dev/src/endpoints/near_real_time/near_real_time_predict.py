import json

from flask import Blueprint, Response, request

from src.algos.mappings import get_config_for_algorithms
from src.algos.mlp import mlp
from src.algos.utils import *

near_real_time_predict = Blueprint('near_real_time', __name__)


@near_real_time_predict.route('/predict/<algorithm_type>/<data_scenario>/<topology_component>/<timestamp>',
                              methods=['POST'])
def predict_near_real_time(algorithm_type, data_scenario, topology_component, timestamp):
    energy_values = extract_energy_values(request.get_json()).reshape(1, -1)

    predictions = sample_predict_near_real_time(energy_values, algorithm_type, data_scenario, topology_component)

    return Response(response=json.dumps(predictions), status=200, mimetype='application/json')


def sample_predict_near_real_time(sample, algorithm_type, data_scenario, topology_component):
    data_scenario = data_scenario.lower()

    mlp_config = get_config_for_algorithms(constants.NEAR_REAL_TIME, data_scenario)
    algorithm_model = mlp.MLP(mlp_config)
    algorithm_model.no_models = 1
    algorithm_model.prediction_type = constants.NEAR_REAL_TIME

    trained_models_root_path = constants.get_trained_models_path(data_scenario, constants.NEAR_REAL_TIME)
    predictions, _ = predict(data=sample,
                             algorithm_type=algorithm_type,
                             algorithm_model=algorithm_model,
                             trained_models_root_path=trained_models_root_path,
                             topology_component=topology_component)

    predictions = np.ravel(predictions).tolist()

    return predictions
