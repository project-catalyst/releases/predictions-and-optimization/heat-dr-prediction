import datetime
import json
import pandas as pd
from flask import Blueprint, request, Response

import src.algos.mappings as mapping
from src.algos.mlp import mlp
from src.algos.utils import *

dayahead_predict = Blueprint('dayahead', __name__)

# TODO: change these to proper, averaged values
SERVER_BASELINE = np.array(
    [1130, 2642, 3284, 3725, 3691, 4151, 5234, 6837, 7057, 4880, 4317, 5308, 6286, 5667, 4931, 5078, 4702, 3615, 3072,
     3763, 3372, 2293, 2261, 995])
SERVER_POZNAN_BASELINE = np.array(
    [1130, 2642, 3284, 3725, 3691, 4151, 5234, 6837, 7057, 4880, 4317, 5308, 6286, 5667, 4931, 5078, 4702, 3615, 3072,
     3763, 3372, 2293, 2261, 995])


@dayahead_predict.route('/predict/<algorithm_type>/<data_scenario>/<topology_component>/<timestamp>', methods=['POST'])
def predict_dayahead(algorithm_type, data_scenario, topology_component, timestamp):
    energy_values = extract_energy_values(request.get_json()).reshape(1, -1)

    predictions = sample_predict_dayahead(energy_values, algorithm_type, data_scenario, topology_component, timestamp)

    return Response(response=json.dumps(predictions), status=200, mimetype='application/json')


# for now topology_component will be the server
@dayahead_predict.route('/predict_flexibility_upper_bounds/<algorithm_type>/<data_scenario>/<topology_component>',
                        methods=['POST'])
def predict_dayahead_flexibility_upper_bounds(algorithm_type, data_scenario, topology_component):
    # flexibility prediction will always be done with MLP TODO: change endpoint or leave it as it is?
    data_scenario = data_scenario.lower()

    algorithm_type = constants.ALGORITHM_TYPE_MLP

    energy_values = extract_energy_values(request.get_json())

    # take only upper bounds of energy values
    upper_bounds = extract_bounds_from_energy_curve(energy_values, constants.FLEXIBILITY_TYPE_UPPER, data_scenario) \
        .reshape(1, -1)

    algo_config = mapping.get_config_for_flexibility_algorithm(constants.DAYAHEAD, constants.FLEXIBILITY_TYPE_UPPER,
                                                               data_scenario)
    algorithm_model = {
        constants.ALGORITHM_TYPE_MLP: mlp.MLP(algo_config)
        # only MLP
    }.get(algorithm_type)
    algorithm_model.no_models = 1
    algorithm_model.prediction_type = constants.DAYAHEAD

    predictions, _ = predict_flexibility(data=upper_bounds,
                                         algorithm_type=algorithm_type,
                                         algorithm_model=algorithm_model,
                                         topology_component=topology_component,
                                         flexibility_type=constants.FLEXIBILITY_TYPE_UPPER,
                                         data_scenario=data_scenario)

    predictions = np.ravel(predictions)
    predictions[predictions < 0] = 0
    predictions = predictions.tolist()

    return Response(response=json.dumps(predictions), status=200, mimetype='application/json')


# for now topology_component will be the server
@dayahead_predict.route('/predict_flexibility_lower_bounds/<algorithm_type>/<data_scenario>/<topology_component>',
                        methods=['POST'])
def predict_dayahead_flexibility_lower_bounds(algorithm_type, data_scenario, topology_component):
    # flexibility prediction will always be done with MLP TODO: change endpoint or leave it as it is?

    data_scenario = data_scenario.lower()

    algorithm_type = constants.ALGORITHM_TYPE_MLP

    energy_values = extract_energy_values(request.get_json())

    # take only lower bounds of energy values
    lower_bounds = extract_bounds_from_energy_curve(energy_values, constants.FLEXIBILITY_TYPE_LOWER, data_scenario) \
        .reshape(1, -1)

    algo_config = mapping.get_config_for_flexibility_algorithm(constants.DAYAHEAD, constants.FLEXIBILITY_TYPE_LOWER,
                                                               data_scenario)
    algorithm_model = {
        constants.ALGORITHM_TYPE_MLP: mlp.MLP(algo_config)
        # only MLP
    }.get(algorithm_type)
    algorithm_model.no_models = 1
    algorithm_model.prediction_type = constants.DAYAHEAD

    predictions, _ = predict_flexibility(data=lower_bounds,
                                         algorithm_type=algorithm_type,
                                         algorithm_model=algorithm_model,
                                         topology_component=topology_component,
                                         flexibility_type=constants.FLEXIBILITY_TYPE_LOWER,
                                         data_scenario=data_scenario)

    predictions = np.ravel(predictions)
    predictions[predictions > 0] = 0
    predictions = predictions.tolist()

    return Response(response=json.dumps(predictions), status=200, mimetype='application/json')


def extract_bounds_from_energy_curve(energy_values, flexibility_type, data_scenario):
    lower_bounds = np.zeros(24)

    baseline = None
    if data_scenario == constants.DATA_SCENARIO[0]:  # Scenario 0 - Paper
        baseline = SERVER_BASELINE
    elif data_scenario == constants.DATA_SCENARIO[1]:  # Scenario 1 - Paper
        baseline = SERVER_POZNAN_BASELINE

    energy_values = energy_values.reshape(24)

    diff = energy_values - baseline
    bounds_value_picker = diff < 0 if flexibility_type == constants.FLEXIBILITY_TYPE_LOWER else diff > 0

    lower_bounds[bounds_value_picker] = diff[bounds_value_picker]

    return lower_bounds


def sample_predict_dayahead(sample, algorithm_type, data_scenario, topology_component, timestamp):
    # pre-process sample data based on scenario
    data_scenario = data_scenario.lower()
    if data_scenario == constants.DATA_SCENARIO[0]:  # Paper
        date_time_obj = datetime.datetime.strptime(timestamp, '%Y-%m-%dT%H:%M')
        is_weekend = date_time_obj.weekday() // 5
        sample = np.append(sample, is_weekend).reshape(1, -1)
    elif data_scenario == constants.DATA_SCENARIO[1]:  # Poznan
        pass

    mlp_config = mapping.get_config_for_algorithms(constants.DAYAHEAD, data_scenario)
    algorithm_model = mlp.MLP(mlp_config)
    algorithm_model.no_models = 1
    algorithm_model.prediction_type = constants.DAYAHEAD

    trained_models_root_path = constants.get_trained_models_path(data_scenario, constants.DAYAHEAD)
    predictions, _ = predict(data=sample,
                             algorithm_type=algorithm_type,
                             algorithm_model=algorithm_model,
                             trained_models_root_path=trained_models_root_path,
                             topology_component=topology_component)

    predictions = np.ravel(predictions).tolist()

    return predictions
