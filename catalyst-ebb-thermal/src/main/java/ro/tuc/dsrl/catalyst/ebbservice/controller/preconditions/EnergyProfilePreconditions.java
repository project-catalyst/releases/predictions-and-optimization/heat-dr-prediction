package ro.tuc.dsrl.catalyst.ebbservice.controller.preconditions;

import ro.tuc.dsrl.catalyst.model.enums.DataScenario;
import ro.tuc.dsrl.catalyst.model.enums.FlexibilityType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionType;
import ro.tuc.dsrl.catalyst.model.error_handler.ErrorMessageConstants;
import ro.tuc.dsrl.catalyst.model.error_handler.IncorrectParameterException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EnergyProfilePreconditions {

    private EnergyProfilePreconditions() {
    }

    public static void validateDatacenter(String dataScenario, String predictionType, String flexibilityType,
                                          String granularity, String dataCenterID, Long startTime) {

        List<String> errors = new ArrayList<>();

        if (dataScenario == null || dataScenario.equals("")) {
            errors.add("Data scenario must not be null.");
        } else if (DataScenario.getDataScenario(dataScenario) == null) {
            String dataScenarioValues = Stream.of(DataScenario.values()).
                    map(DataScenario::getScenario).
                    collect(Collectors.joining("or "));

            errors.add("Data scenario must be PAPER or POZNAN." + dataScenarioValues);
        }

        if (predictionType == null || predictionType.equals("")) {
            errors.add("Prediction type must not be null.");
        } else if (!predictionType.equals(PredictionType.ENERGY_CONSUMPTION.getType()) &&
                !predictionType.equals(PredictionType.ENERGY_PRODUCTION.getType())) {
            errors.add("Prediction must be PRODUCTION or CONSUMPTION");
        }

        if (flexibilityType == null || flexibilityType.equals("")) {
            errors.add("Flexibility type must not be null");
        } else if (!flexibilityType.equals(FlexibilityType.NONE.getType()) &&
                !flexibilityType.equals(FlexibilityType.UPPER.getType()) &&
                !flexibilityType.equals(FlexibilityType.LOWER.getType())) {
            errors.add("Flexibility must be NONE or UPPER or LOWER");
        }

        if (granularity == null || granularity.equals("")) {
            errors.add(ErrorMessageConstants.GRANULARITY_NOT_NULL);
        }

        String dataCenterIDValidationError = UUIDPreconditions.validate(dataCenterID);
        if (dataCenterID == null) {
            errors.add(dataCenterIDValidationError);
        }

        if (startTime == null) {
            errors.add("Start time must not be null");
        }

        if (!errors.isEmpty()) {
            throw new IncorrectParameterException("Incorrect parameters", errors);
        }
    }

    public static void validateComponent(String dataScenario, String granularity, String dataCenterID,
                                         String componentType, String componentID, Long startTime) {

        List<String> errors = new ArrayList<>();

        if (dataScenario == null || dataScenario.equals("")) {
            errors.add("Data scenario must not be null.");
        } else if (DataScenario.getDataScenario(dataScenario) == null) {
            errors.add("Data scenario must be PAPER or POZNAN.");
        }

        if (granularity == null || granularity.equals("")) {
            errors.add("Granularity must not be null");
        }

        String dataCenterIDValidationError = UUIDPreconditions.validate(dataCenterID);
        if (dataCenterIDValidationError != null) {
            errors.add(dataCenterIDValidationError);
        }

        if (componentType == null || componentType.equals("")) {
            errors.add("Granularity must not be null");
        }

        String componentIDValidationError = UUIDPreconditions.validate(componentID);
        if (componentIDValidationError != null) {
            errors.add(componentIDValidationError);
        }

        if (startTime == null) {
            errors.add("Start time must not be null");
        }

        if (!errors.isEmpty()) {
            throw new IncorrectParameterException("Incorrect parameters", errors);
        }
    }
}
