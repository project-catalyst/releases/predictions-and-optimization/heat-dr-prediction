package ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces;

import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.ThermalDTO;
import ro.tuc.dsrl.catalyst.model.dto.ThermalSimulationDTO;
import ro.tuc.dsrl.catalyst.model.enums.DataScenario;
import ro.tuc.dsrl.catalyst.model.enums.FlexibilityType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.enums.TopologyComponentType;

import java.time.LocalDateTime;

public interface IAlgorithmAccess {

    EnergyProfileDTO computePrediction(EnergyProfileDTO curve,
                                       PredictionGranularity granularity,
                                       DataScenario dataScenario,
                                       LocalDateTime startTime,
                                       TopologyComponentType componentType,
                                       FlexibilityType flexibilityType);

    ThermalSimulationDTO computePredictionTemperature(ThermalDTO thermalDTO);
}
