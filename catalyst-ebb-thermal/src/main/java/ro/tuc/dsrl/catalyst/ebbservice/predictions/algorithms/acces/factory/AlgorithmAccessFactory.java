package ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.factory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.IAlgorithmAccess;

import java.util.Map;

@Component
public class AlgorithmAccessFactory {

    private Map<String, IAlgorithmAccess> algorithmsImplementations;

    @Autowired
    public AlgorithmAccessFactory(Map<String, IAlgorithmAccess> algorithmsImplementations) {
        this.algorithmsImplementations = algorithmsImplementations;
    }

    public IAlgorithmAccess getImplementation(String key) {
        String beanName = AlgorithmType.getBeanNameForKey(key);
        return algorithmsImplementations.get(beanName);
    }
}
