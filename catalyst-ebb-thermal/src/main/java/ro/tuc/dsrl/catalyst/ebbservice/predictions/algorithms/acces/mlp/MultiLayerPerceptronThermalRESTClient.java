package ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.mlp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.IAlgorithmRESTClient;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.factory.AlgorithmType;
import ro.tuc.dsrl.catalyst.ebbservice.remote_connection.RestAPIClient;
import ro.tuc.dsrl.catalyst.ebbservice.util.PredictionApi;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.ProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.ThermalDTO;
import ro.tuc.dsrl.catalyst.model.enums.TopologyComponentType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

//@Component
public class MultiLayerPerceptronThermalRESTClient {//implements IAlgorithmRESTClient<EnergyProfileDTO, List<Double>> {
/*
    private static final Logger LOGGER = Logger.getLogger(MultiLayerPerceptronRESTClient.class.getName());

    private final RestAPIClient restAccessPoint;

    @Autowired
    public MultiLayerPerceptronThermalRESTClient(RestAPIClient restAccessPoint) {
        this.restAccessPoint = restAccessPoint;
    }


    @Override
    public List<Double> predictEnergyValues(EnergyProfileDTO energyProfileDTO, PredictionApi predictionApi, AlgorithmType algorithmType, TopologyComponentType componentType) {
        return null;
    }
    @Override
    public ProfileDTO predictTemperatureValues(
            PredictionApi predictionApi,
            ThermalDTO thermalDTO) {
        LOGGER.info("Call of MLP THERMAL for predicting...");
        /**
         * @app.route("/compute/<rack1_load>/<rack2_load>/<air_t>/<air_f>/<init_temp>", methods=['GET'])
          def compute(rack1_load, rack2_load, air_t, air_f, init_temp):
         * */
/*
        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put("rack1_load", thermalDTO.getTemperatureRack1());
        urlVariables.put("rack2_load", thermalDTO.getTemperatureRack2());
        urlVariables.put("air_t", thermalDTO.getTemperatureInputAir());
        urlVariables.put("air_f", thermalDTO.getAirFlow());
        urlVariables.put("init_temp", thermalDTO.getTemperatureRoomInitial());

        return  restAccessPoint.getObject(
                predictionApi.getApiUrl(), ProfileDTO.class,
                urlVariables);
    }*/
}
