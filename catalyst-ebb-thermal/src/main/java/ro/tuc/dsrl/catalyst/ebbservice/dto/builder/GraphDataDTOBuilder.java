package ro.tuc.dsrl.catalyst.ebbservice.dto.builder;

import ro.tuc.dsrl.catalyst.model.dto.*;

import java.util.ArrayList;
import java.util.Set;

public class GraphDataDTOBuilder {

    public static GraphDataDTO fromThermalSimulationDTO(ThermalSimulationDTO thermalSimulationDTO){
        GraphDataDTO graphDataDTO = new GraphDataDTO(10);
        Set<String> keys = thermalSimulationDTO.getThermalSimulations().keySet();
        for(String s:keys){
            ThermalProfileDTO thermalProfileDTO = thermalSimulationDTO.getThermalSimulations().get(s);
            ArrayList<Double> tempProfile = new ArrayList<Double>();
            for(EnergySampleDTO e: thermalProfileDTO.getTemperature()){
                double aux = e.getEnergyValue();
                tempProfile.add(aux);
            }
            LabeledValues labeledValues = new LabeledValues(s, tempProfile);
            graphDataDTO.addLabeledValues(labeledValues);
        }

        return graphDataDTO;
    }

}
