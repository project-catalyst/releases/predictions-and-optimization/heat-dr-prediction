package ro.tuc.dsrl.catalyst.ebbservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.dsrl.catalyst.ebbservice.controller.preconditions.EnergyProfilePreconditions;
import ro.tuc.dsrl.catalyst.ebbservice.dto.builder.GraphDataDTOBuilder;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.AlgorithmsBroker;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.GraphDataDTO;
import ro.tuc.dsrl.catalyst.model.dto.ThermalDTO;
import ro.tuc.dsrl.catalyst.model.dto.ThermalSimulationDTO;
import ro.tuc.dsrl.catalyst.model.enums.*;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;

import java.util.UUID;

@RestController
@RequestMapping("/thermal/prediction")
@CrossOrigin
public class ThermalPredictionController {

    private final AlgorithmsBroker algorithmsBroker;

    @Autowired
    public ThermalPredictionController(AlgorithmsBroker algorithmsBroker) {
        this.algorithmsBroker = algorithmsBroker;
    }

    @GetMapping(value =
            "/{data-scenario}/{prediction-type}/{flexibility-type}/{granularity}/{dataCenterID}/entireDC/{startTime}")
    public EnergyProfileDTO getEnergyPredictionForEntireDC(
            @PathVariable("data-scenario") String dataScenario,
            @PathVariable("prediction-type") String predictionType,
            @PathVariable ("flexibility-type") String flexibilityType,
            @PathVariable("granularity") String granularity,
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("startTime") Long startTime) {

        EnergyProfilePreconditions.validateDatacenter(dataScenario, predictionType, flexibilityType, granularity,
                dataCenterID, startTime);

        return algorithmsBroker.computeEnergyPrediction(
                DataScenario.getDataScenario(dataScenario),
                PredictionType.setType(predictionType),
                PredictionGranularity.valueOf(granularity.toUpperCase()),
                UUID.fromString(dataCenterID),
                TopologyComponentType.DATACENTER,
                UUID.fromString(dataCenterID),
                DateUtils.millisToLocalDateTime(startTime),
                FlexibilityType.setType(flexibilityType));
    }

    /* TODO: we can write only a single method
        we do not use componentID when retrieving data
        - use only a single method!
        - delete endTime and componentID*/
    @GetMapping(value = "/{data-scenario}/{prediction-type}/{flexibility-type}/{granularity}/{dataCenterID}/{component-type}/{component-id}/{startTime}/{endTime}")
    public EnergyProfileDTO getEnergyPredictionForComponent(
            @PathVariable("data-scenario") String dataScenario,
            @PathVariable("prediction-type") String predictionType,
            @PathVariable("flexibility-type") String flexibilityType,
            @PathVariable("granularity") String granularity,
            @PathVariable("dataCenterID") String dataCenterID,
            @PathVariable("component-type") String componentType,
            @PathVariable("component-id") String componentID,
            @PathVariable("startTime") Long startTime) {

        EnergyProfilePreconditions.validateComponent(dataScenario, granularity, dataCenterID, componentType,
                componentID, startTime);

        return algorithmsBroker.computeEnergyPrediction(
                DataScenario.getDataScenario(dataScenario),
                PredictionType.setType(predictionType),
                PredictionGranularity.valueOf(granularity.toUpperCase()),
                UUID.fromString(dataCenterID),
                TopologyComponentType.setType(componentType),
                UUID.fromString(componentID),
                DateUtils.millisToLocalDateTime(startTime),
                FlexibilityType.setType(flexibilityType));
    }

    /*
    * inlet probe 1 rack 1
    * inlet probe 2 rack 1
    * inlet probe 3 rack 1
    * inlet probe 4 rack 1
    * inlet probe 1 rack 2
    * inlet probe 2 rack 2
    * inlet probe 3 rack 2
    * inlet probe 4 rack 2
    * outlet probe 1 rack 1
    * outlet probe 1 rack 2
    * outlet probe 1 room
    * outlet probe 2 rack 1
    * outlet probe 2 rack 2
    * outlet probe 2 room
    *
    * */
    @PostMapping(value = "/near-real-time-thermal")
    public ThermalSimulationDTO computeNearRealTimeThermal(@RequestBody ThermalDTO thermalDTO) {
        return algorithmsBroker.computeThermalPrediction(thermalDTO);
    }
    @CrossOrigin
    @GetMapping(value = "/near-real-time-thermal-prediction/{rack1_load}/{rack2_load}/{air_t}/{air_f}/{init_temp}")
    public GraphDataDTO computeNearRealTimeThermalPrediction(@PathVariable("rack1_load") Double rack1_load,
                                                             @PathVariable("rack2_load") Double rack2_load,
                                                             @PathVariable("air_t") Double air_t,
                                                             @PathVariable("air_f") Double air_f,
                                                             @PathVariable("init_temp") Double init_temp){
        Long start = 1564489487245L;
        ThermalDTO thermalDTO = new ThermalDTO(start, rack1_load,rack2_load,air_t, air_f, init_temp);
        ThermalSimulationDTO thermalSimulationDTO = algorithmsBroker.computeThermalPrediction(thermalDTO);
        GraphDataDTO graphDataDTO = GraphDataDTOBuilder.fromThermalSimulationDTO(thermalSimulationDTO);
        return graphDataDTO;
    }


}
