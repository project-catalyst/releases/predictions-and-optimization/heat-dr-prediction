package ro.tuc.dsrl.catalyst.ebbservice.remote_connection.url;

import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.enums.PredictionType;
import ro.tuc.dsrl.catalyst.model.enums.TopologyComponentType;

public enum DbApiUrl implements ApiUrl {

    GET_PRODUCTION_ENERGY_CURVE_FOR_ENTIREDC_24HOURS_BEFORE("electrical/history/monitored-values/production/24hoursbefore/{dataCenterID}/entireDC/{startTime}"),
    GET_PRODUCTION_ENERGY_CURVE_FOR_ENTIREDC_4HOURS_BEFORE("electrical/history/monitored-values/production/4hoursbefore/{dataCenterID}/entireDC/{startTime}"),
    GET_PRODUCTION_ENERGY_CURVE_FOR_ENTIREDC_1HOUR_BEFORE("electrical/history/monitored-values/production/1hourbefore/{dataCenterID}/entireDC/{startTime}"),

    GET_CONSUMPTION_ENERGY_CURVE_FOR_ENTIREDC_24HOURS_BEFORE("electrical/history/monitored-values/consumption/24hoursbefore/{dataCenterID}/entireDC/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_ENTIREDC_4HOURS_BEFORE("electrical/history/monitored-values/consumption/4hoursbefore/{dataCenterID}/entireDC/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_ENTIREDC_1HOUR_BEFORE("electrical/history/monitored-values/consumption/1hourbefore/{dataCenterID}/entireDC/{startTime}"),

    GET_CONSUMPTION_ENERGY_CURVE_FOR_ITCOMPONENT_24HOURS_BEFORE("electrical/history/monitored-values/consumption/24hoursbefore/{dataCenterID}/itComponent/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_ITCOMPONENT_4HOURS_BEFORE("electrical/history/monitored-values/consumption/4hoursbefore/{dataCenterID}/itComponent/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_ITCOMPONENT_1HOUR_BEFORE("electrical/history/monitored-values/consumption/1hourbefore/{dataCenterID}/itComponent/{startTime}"),

    GET_CONSUMPTION_ENERGY_CURVE_FOR_COOLING_SYSTEM_24HOURS_BEFORE("electrical/history/monitored-values/consumption/24hoursbefore/{dataCenterID}/coolingSystem/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_COOLING_SYSTEM_4HOURS_BEFORE("electrical/history/monitored-values/consumption/4hoursbefore/{dataCenterID}/coolingSystem/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_COOLING_SYSTEM_1HOUR_BEFORE("electrical/history/monitored-values/consumption/1hourbefore/{dataCenterID}/coolingSystem/{startTime}"),

    GET_CONSUMPTION_ENERGY_CURVE_FOR_SERVER_24HOURS_BEFORE("electrical/history/monitored-values/consumption/24hoursbefore/{dataCenterID}/server/{serverID}/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_SERVER_4HOURS_BEFORE("electrical/history/monitored-values/production/4hoursbefore/{dataCenterID}/server/{serverID}/{startTime}"),
    GET_CONSUMPTION_ENERGY_CURVE_FOR_SERVER_1HOUR_BEFORE("electrical/history/monitored-values/production/1hourbefore/{dataCenterID}/server/{serverID}/{startTime}"),

    POST_PREDICTED_ENERGY_CURVE("predicted-value/insert_predicted_energy_curve/{predictionJobID}"),
    POST_PREDICTION_JOB("prediction-job/insert");

    private String value;

    DbApiUrl(String url) {
        this.value = url;
    }

    @Override
    public String value() {
        return value;
    }

    public static DbApiUrl getValue(PredictionType predictionType, PredictionGranularity predictionGranularity) {
        switch (predictionType) {
            case ENERGY_PRODUCTION:

                return getProductionEndpointByPredictionGranularity(predictionGranularity);

            case ENERGY_CONSUMPTION:

                return getConsumptionEndpointByPredictionGranularity(predictionGranularity);

            default:

                return null;
        }
    }

    public static DbApiUrl getValue(PredictionGranularity predictionGranularity, TopologyComponentType component) {
        switch (predictionGranularity) {

            case DAYAHEAD:
                switch (component) {

                    case COOLING_SYSTEM:
                        return GET_CONSUMPTION_ENERGY_CURVE_FOR_COOLING_SYSTEM_24HOURS_BEFORE;
                    case IT_COMPONENT:
                        return GET_CONSUMPTION_ENERGY_CURVE_FOR_ITCOMPONENT_24HOURS_BEFORE;
                    case SERVER:
                        return GET_CONSUMPTION_ENERGY_CURVE_FOR_SERVER_24HOURS_BEFORE;
                    default:
                        return null;
                }
            case INTRADAY:
                switch (component) {

                    case COOLING_SYSTEM:
                        return GET_CONSUMPTION_ENERGY_CURVE_FOR_COOLING_SYSTEM_4HOURS_BEFORE;
                    case IT_COMPONENT:
                        return GET_CONSUMPTION_ENERGY_CURVE_FOR_ITCOMPONENT_4HOURS_BEFORE;
                    case SERVER:
                        return GET_CONSUMPTION_ENERGY_CURVE_FOR_SERVER_4HOURS_BEFORE;
                    default:
                        return null;
                }
            case NEAR_REAL_TIME:
                switch (component) {

                    case COOLING_SYSTEM:
                        return GET_CONSUMPTION_ENERGY_CURVE_FOR_COOLING_SYSTEM_1HOUR_BEFORE;
                    case IT_COMPONENT:
                        return GET_CONSUMPTION_ENERGY_CURVE_FOR_ITCOMPONENT_1HOUR_BEFORE;
                    case SERVER:
                        return GET_CONSUMPTION_ENERGY_CURVE_FOR_SERVER_1HOUR_BEFORE;
                    default:
                        return null;
                }
            default:
                return null;
        }
    }


    private static DbApiUrl getProductionEndpointByPredictionGranularity(PredictionGranularity predictionGranularity){

        switch (predictionGranularity) {
            case DAYAHEAD:
                return GET_PRODUCTION_ENERGY_CURVE_FOR_ENTIREDC_24HOURS_BEFORE;
            case INTRADAY:
                return GET_PRODUCTION_ENERGY_CURVE_FOR_ENTIREDC_4HOURS_BEFORE;
            case NEAR_REAL_TIME:
                return GET_PRODUCTION_ENERGY_CURVE_FOR_ENTIREDC_1HOUR_BEFORE;
            default:
                return null;
        }
    }

    private static DbApiUrl getConsumptionEndpointByPredictionGranularity(PredictionGranularity predictionGranularity){

        switch (predictionGranularity) {
            case DAYAHEAD:
                return GET_CONSUMPTION_ENERGY_CURVE_FOR_ENTIREDC_24HOURS_BEFORE;
            case INTRADAY:
                return GET_CONSUMPTION_ENERGY_CURVE_FOR_ENTIREDC_4HOURS_BEFORE;
            case NEAR_REAL_TIME:
                return GET_CONSUMPTION_ENERGY_CURVE_FOR_ENTIREDC_1HOUR_BEFORE;
            default:
                return null;
        }
    }

}
