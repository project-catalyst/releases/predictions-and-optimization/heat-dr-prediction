package ro.tuc.dsrl.catalyst.ebbservice.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ro.tuc.dsrl.catalyst.ebbservice.remote_connection.RestAPIClient;
import ro.tuc.dsrl.catalyst.ebbservice.remote_connection.url.DbApiUrl;
import ro.tuc.dsrl.catalyst.ebbservice.util.Constants;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.PredictionJobDTO;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.enums.PredictionType;
import ro.tuc.dsrl.catalyst.model.enums.TopologyComponentType;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Repository
public class EnergyProfileRepository {

    private final RestAPIClient accessPoint;

    @Autowired
    public EnergyProfileRepository(RestAPIClient accessPoint) {
        this.accessPoint = accessPoint;
    }

    public void insert(EnergyProfileDTO energyProfileDTO, PredictionJobDTO predictionJobDTO) {

        UUID predictionJobID = accessPoint.postObject(DbApiUrl.POST_PREDICTION_JOB, predictionJobDTO, UUID.class);

        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put("predictionJobID", predictionJobID);
        accessPoint.postObject(
                DbApiUrl.POST_PREDICTED_ENERGY_CURVE,
                energyProfileDTO,
                Boolean.class,
                urlVariables);
    }

    public EnergyProfileDTO getEnergyProfileMonitoredValues(
            PredictionType predictionType,
            PredictionGranularity predictionGranularity,
            UUID dataCenterID,
            TopologyComponentType componentType,
            UUID componentID,
            LocalDateTime startTime) {

        Map<String, Object> urlVariables = new HashMap<>();
        urlVariables.put(Constants.START_TIME, DateUtils.localDateTimeToLongInMillis(startTime));
        urlVariables.put(Constants.DATACENTER_ID, dataCenterID);

        switch(componentType){
            case DATACENTER:
                return accessPoint.getObject(
                        DbApiUrl.getValue(predictionType, predictionGranularity),
                        EnergyProfileDTO.class,
                        urlVariables
                );

            case SERVER:
                urlVariables.put(Constants.SERVER_ID, componentID);
                return accessPoint.getObject(
                        DbApiUrl.getValue(predictionGranularity, componentType),
                        EnergyProfileDTO.class,
                        urlVariables);

            default:
                return accessPoint.getObject(
                        DbApiUrl.getValue(predictionGranularity, componentType),
                        EnergyProfileDTO.class,
                        urlVariables);
        }
    }
}