package ro.tuc.dsrl.catalyst.ebbservice.predictions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.IAlgorithmAccess;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.factory.AlgorithmAccessFactory;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.factory.AlgorithmType;
import ro.tuc.dsrl.catalyst.ebbservice.service.EnergyProfileService;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.PredictionJobDTO;
import ro.tuc.dsrl.catalyst.model.dto.ThermalDTO;
import ro.tuc.dsrl.catalyst.model.dto.ThermalSimulationDTO;
import ro.tuc.dsrl.catalyst.model.enums.*;

import java.time.LocalDateTime;
import java.util.UUID;
import java.util.logging.Logger;

@Component
public class AlgorithmsBroker {

    private static final Logger LOGGER = Logger.getLogger(AlgorithmsBroker.class.getName());

    private final EnergyProfileService energyProfileService;
    private final AlgorithmAccessFactory algorithmFactory;

    @Autowired
    public AlgorithmsBroker(EnergyProfileService energyProfileService, AlgorithmAccessFactory algorithmFactory) {
        this.energyProfileService = energyProfileService;
        this.algorithmFactory = algorithmFactory;
    }

    public EnergyProfileDTO computeEnergyPrediction(
            DataScenario dataScenario,
            PredictionType predictionType,
            PredictionGranularity predictionGranularity,
            UUID dataCenterID,
            TopologyComponentType componentType,
            UUID componentID,
            LocalDateTime startTime,
            FlexibilityType flexibilityType) {

        // Set algortihmType to MLP
        AlgorithmType algorithmType = AlgorithmType.MLP;
        //--------------------------------PREDICTING DATA PART--------------------------------------------------------//
        LOGGER.info("Requesting an energy curve from the monitored data as the input for the prediction...");
        EnergyProfileDTO energyProfile = energyProfileService.getEnergyProfile(
                predictionType,
                predictionGranularity,
                dataCenterID,
                componentType,
                componentID,
                startTime.minusHours(predictionGranularity.getNoHours())
        );

        LOGGER.info("The Algorithm access is going to compute the prediction...");
        IAlgorithmAccess algorithmAccess = algorithmFactory.getImplementation(algorithmType.name());
        EnergyProfileDTO predictedEnergyProfile = algorithmAccess.computePrediction(
                energyProfile,
                predictionGranularity,
                dataScenario,
                startTime,
                componentType,
                flexibilityType);
        //------------------------------------------------------------------------------------------------------------//

        LOGGER.info("The energy curve predicted (the output of the prediction) is being stored in DB");
        // insert predicted values & prediction job
        PredictionJobDTO predictionJobDTO = new PredictionJobDTO(UUID.randomUUID(),
                LocalDateTime.now(),
                predictionGranularity.getGranularity(),
                algorithmType.name(),
                flexibilityType.getType());
//        energyProfileService.postEnergyProfilePredicted(predictedEnergyProfile, predictionJobDTO);

        return predictedEnergyProfile;
    }

    public ThermalSimulationDTO computeThermalPrediction(ThermalDTO thermalDTO){
        // Set algortihmType to MLP
        AlgorithmType algorithmType = AlgorithmType.MLP;
        IAlgorithmAccess algorithmAccess = algorithmFactory.getImplementation(algorithmType.name());
        ThermalSimulationDTO predictedTemperatureProfile = algorithmAccess.computePredictionTemperature(thermalDTO);
        return predictedTemperatureProfile;
    }
}
