package ro.tuc.dsrl.catalyst.ebbservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.dsrl.catalyst.ebbservice.repository.EnergyProfileRepository;
import ro.tuc.dsrl.catalyst.ebbservice.service.validators.EnergyProfileValidator;
import ro.tuc.dsrl.catalyst.ebbservice.service.validators.PredictionJobValidator;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.PredictionJobDTO;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.enums.PredictionType;
import ro.tuc.dsrl.catalyst.model.enums.TopologyComponentType;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
public class EnergyProfileService {

    private final EnergyProfileRepository energyProfileRepository;

    @Autowired
    public EnergyProfileService(EnergyProfileRepository energyProfileRepository) {
        this.energyProfileRepository = energyProfileRepository;
    }

    public void postEnergyProfilePredicted(EnergyProfileDTO energyProfileDTO, PredictionJobDTO predictionJobDTO) {

        PredictionJobValidator.validatePredictionJob(predictionJobDTO);
        EnergyProfileValidator.validatePredictedEnergyProfile(energyProfileDTO);

        energyProfileRepository.insert(energyProfileDTO, predictionJobDTO);
    }

    public EnergyProfileDTO getEnergyProfile(PredictionType predictionType,
            PredictionGranularity predictionGranularity,
            UUID dataCenterID,
            TopologyComponentType componentType,
            UUID componentID,
            LocalDateTime startTime) {

        EnergyProfileValidator.validateEnergyProfileParams(predictionType, predictionGranularity,
                dataCenterID, componentType, componentID, startTime);

        return energyProfileRepository.getEnergyProfileMonitoredValues(
                predictionType,
                predictionGranularity,
                dataCenterID,
                componentType,
                componentID,
                startTime);
    }

}
