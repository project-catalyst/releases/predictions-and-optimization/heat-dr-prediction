package ro.tuc.dsrl.catalyst.ebbservice;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ScheduledThreadPoolExecutor;

@Configuration
public class ThreadConfig {

    @Bean
    public ScheduledThreadPoolExecutor threadPoolExecutor() {

        return new ScheduledThreadPoolExecutor(10);
    }
}
