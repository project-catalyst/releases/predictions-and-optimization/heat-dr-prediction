package ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.mlp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.tuc.dsrl.catalyst.ebbservice.dto.builder.ThermalSimulationDTOBuilder;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.IAlgorithmAccess;
import ro.tuc.dsrl.catalyst.ebbservice.util.PredictionApi;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.ProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.ThermalDTO;
import ro.tuc.dsrl.catalyst.model.dto.ThermalSimulationDTO;
import ro.tuc.dsrl.catalyst.model.enums.FlexibilityType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.enums.TopologyComponentType;

import java.time.LocalDateTime;
import java.util.List;
import java.util.logging.Logger;

//@Component("mlp-thermal-access")
public class MultiLayerPerceptronThermalAccess {//implements IAlgorithmAccess {
/*
    private static final Logger LOGGER = Logger.getLogger(MultiLayerPerceptronAccess.class.getName());

    private final MultiLayerPerceptronThermalRESTClient algorithmRESTClient;

    @Autowired
    public MultiLayerPerceptronThermalAccess(MultiLayerPerceptronThermalRESTClient algorithmRESTClient) {
        this.algorithmRESTClient = algorithmRESTClient;
    }

    @Override
    public EnergyProfileDTO computePrediction(EnergyProfileDTO curve, PredictionGranularity granularity, LocalDateTime startTime, TopologyComponentType componentType, FlexibilityType flexibilityType) {
        return null;
    }
    @Override
    public ThermalSimulationDTO computePredictionTemperature(ThermalDTO thermalDTO){
        LOGGER.info("MLP Thermal is computing prediction...");
        ProfileDTO temperaturePredictedValues = algorithmRESTClient.predictTemperatureValues(
                PredictionApi.THERMAL_REAL_TIME,
                thermalDTO);
        return ThermalSimulationDTOBuilder.fromTemperatureData(thermalDTO.getStartTime(), temperaturePredictedValues);


    }

*/
}
