package ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.mlp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ro.tuc.dsrl.catalyst.ebbservice.dto.builder.EnergyProfileDTOBuilder;
import ro.tuc.dsrl.catalyst.ebbservice.dto.builder.ThermalSimulationDTOBuilder;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.IAlgorithmAccess;
import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.factory.AlgorithmType;
import ro.tuc.dsrl.catalyst.ebbservice.util.PredictionApi;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.ProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.ThermalDTO;
import ro.tuc.dsrl.catalyst.model.dto.ThermalSimulationDTO;
import ro.tuc.dsrl.catalyst.model.enums.DataScenario;
import ro.tuc.dsrl.catalyst.model.enums.FlexibilityType;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.enums.TopologyComponentType;
import ro.tuc.dsrl.catalyst.model.utils.DateUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.logging.Logger;

@Component("mlp-access")
public class MultiLayerPerceptronAccess implements IAlgorithmAccess {

    private static final Logger LOGGER = Logger.getLogger(MultiLayerPerceptronAccess.class.getName());

    private final MultiLayerPerceptronRESTClient algorithmRESTClient;

    @Autowired
    public MultiLayerPerceptronAccess(MultiLayerPerceptronRESTClient algorithmRESTClient) {
        this.algorithmRESTClient = algorithmRESTClient;
    }

    @Override
    public EnergyProfileDTO computePrediction(
            EnergyProfileDTO energyProfileDTO,
            PredictionGranularity granularity,
            DataScenario dataScenario,
            LocalDateTime startTime,
            TopologyComponentType componentType,
            FlexibilityType flexibilityType) {

        LOGGER.info("MLP is computing prediction...");

        List<Double> energyPredictedValues = algorithmRESTClient.predictEnergyValues(
                energyProfileDTO,
                PredictionApi.getPredictionApiByPredictionGranularityAndFlexibilityType(granularity, flexibilityType),
                dataScenario,
                AlgorithmType.MLP,
                componentType,
                startTime.toString());

        return EnergyProfileDTOBuilder.fromPredictionOutputData(
                energyPredictedValues,
                energyProfileDTO,
                startTime);
    }

    @Override
    public ThermalSimulationDTO computePredictionTemperature(ThermalDTO thermalDTO){
        LOGGER.info("MLP Thermal is computing prediction...");
        ProfileDTO temperaturePredictedValues = algorithmRESTClient.predictTemperatureValues(
                PredictionApi.THERMAL_REAL_TIME,
                thermalDTO);
        return ThermalSimulationDTOBuilder.fromTemperatureData(DateUtils.millisToLocalDateTime(thermalDTO.getStartTime()), temperaturePredictedValues);


    }
}
