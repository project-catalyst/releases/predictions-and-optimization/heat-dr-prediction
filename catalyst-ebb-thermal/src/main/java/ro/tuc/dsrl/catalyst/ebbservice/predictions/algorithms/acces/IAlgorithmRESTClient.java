package ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces;

import ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.factory.AlgorithmType;
import ro.tuc.dsrl.catalyst.ebbservice.util.PredictionApi;
import ro.tuc.dsrl.catalyst.model.dto.ProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.ThermalDTO;
import ro.tuc.dsrl.catalyst.model.enums.DataScenario;
import ro.tuc.dsrl.catalyst.model.enums.TopologyComponentType;

import java.util.List;

public interface IAlgorithmRESTClient<T, O> {

    O predictEnergyValues(T t, PredictionApi predictionApi, DataScenario dataScenario, AlgorithmType algorithmType,
                          TopologyComponentType componentType, String startTime);
    ProfileDTO predictTemperatureValues(PredictionApi predictionApi, ThermalDTO thermalDTO);
}
