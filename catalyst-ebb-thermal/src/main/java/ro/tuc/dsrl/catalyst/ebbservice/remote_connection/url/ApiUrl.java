package ro.tuc.dsrl.catalyst.ebbservice.remote_connection.url;

public interface ApiUrl {

    String value();
}

