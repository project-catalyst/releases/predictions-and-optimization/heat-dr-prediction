package ro.tuc.dsrl.catalyst.ebbservice.controller.preconditions;

import java.util.regex.Pattern;

public class UUIDPreconditions {

    private static final Pattern VALID_UUID_REGEX =
            Pattern.compile("\\b[0-9a-f]{8}\\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\\b[0-9a-f]{12}\\b",
                    Pattern.CASE_INSENSITIVE);

    private UUIDPreconditions(){

    }

    public static String validate(String uuid) {

        if (uuid == null) {
            return ("ID must not be null");
        } else {
            if (!VALID_UUID_REGEX.matcher(uuid).find()) {
                return ("ID does not have the correct format");
            }
        }

        return null;
    }
}
