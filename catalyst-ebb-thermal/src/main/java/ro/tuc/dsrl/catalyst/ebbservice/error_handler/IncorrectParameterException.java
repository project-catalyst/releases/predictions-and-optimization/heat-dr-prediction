package ro.tuc.dsrl.catalyst.ebbservice.error_handler;

import java.io.Serializable;
import java.util.List;

public class IncorrectParameterException extends RuntimeException implements Serializable {

    private static final long serialVersionUID = 2405172041950251807L;

    private static final String MESSAGE = "Incorrect parameters";

    private final String resource;
    private final List<String> invalidParams;

    public IncorrectParameterException(String resource, List<String> errors) {
        super(MESSAGE);
        this.resource = resource;
        this.invalidParams = errors;
    }

    public List<String> getInvalidParams() {
        return invalidParams;
    }

    public String getResource() {
            return resource;
    }

}

