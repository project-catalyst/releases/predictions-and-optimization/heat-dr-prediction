package ro.tuc.dsrl.catalyst.ebbservice.predictions.algorithms.acces.factory;

public enum AlgorithmType {

    MLP("mlp-access", "mlp"),
    MLP_THERMAL("mlp-thermal-access", "mlp-thermal");

    private String beanName;
    private String algorithmName;

    AlgorithmType(String beanName, String algorithmName) {
        this.beanName = beanName;
        this.algorithmName = algorithmName;
    }

    public static String getBeanNameForKey(String key) {

        AlgorithmType[] values = AlgorithmType.values();
        for (AlgorithmType mapping : values) {
            if (mapping.name().equals(key))
                return mapping.beanName;
        }

        return "";
    }

    public String getAlgorithmName() {
        return algorithmName;
    }
}
