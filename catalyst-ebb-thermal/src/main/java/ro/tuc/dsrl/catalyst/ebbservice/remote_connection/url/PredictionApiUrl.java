package ro.tuc.dsrl.catalyst.ebbservice.remote_connection.url;

public enum PredictionApiUrl implements ApiUrl {

    POST_NEAR_REALTIME_PREDICTION("near_real_time/predict/{algorithmType}/{dataScenario}/{componentType}/{timestamp}"),
    POST_INTRADAY_PREDICTION("intraday/predict/{algorithmType}/{dataScenario}/{componentType}/{timestamp}"),
    POST_DAYAHEAD_PREDICTION("dayahead/predict/{algorithmType}/{dataScenario}/{componentType}/{timestamp}"),
    POST_DAYAHEAD_UPPER_FLEXIBILITY("dayahead/predict_flexibility_upper_bounds/{algorithmType}/{dataScenario}/{componentType}"),
    POST_DAYAHEAD_LOWER_FLEXIBILITY("dayahead/predict_flexibility_lower_bounds/{algorithmType}/{dataScenario}/{componentType}"),
    POST_THERMAL_NEAR_REAL_TIME("http://dsrl.fairuse.org:9093/compute/{rack1_load}/{rack2_load}/{air_t}/{air_f}/{init_temp}");

    private String value;

    PredictionApiUrl(String url) {
        this.value = url;
    }

    public String value() {
        return value;
    }
}
