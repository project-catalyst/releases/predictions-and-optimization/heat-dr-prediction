package ro.tuc.dsrl.catalyst.ebbservice.service.validators;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.catalyst.ebbservice.error_handler.IncorrectParameterException;
import ro.tuc.dsrl.catalyst.model.dto.EnergyProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.EnergySampleDTO;
import ro.tuc.dsrl.catalyst.model.enums.PredictionGranularity;
import ro.tuc.dsrl.catalyst.model.enums.PredictionType;
import ro.tuc.dsrl.catalyst.model.enums.TopologyComponentType;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class EnergyProfileValidator {

    private static final Log LOGGER = LogFactory.getLog(EnergyProfileValidator.class);

    private EnergyProfileValidator(){

    }

    public static void validatePredictedEnergyProfile(EnergyProfileDTO energyProfileDTO) {

        List<String> errors = new ArrayList<>();

        if (energyProfileDTO == null) {
            errors.add("EnergyProfileDTO must not be null.");

            LOGGER.error(errors);
            throw new IncorrectParameterException(EnergyProfileValidator.class.getSimpleName(), errors);
        }

        List<EnergySampleDTO> curve = energyProfileDTO.getCurve();
        if (curve == null || energyProfileDTO.getCurve().isEmpty()) {
            errors.add("Energy curve is null or empty.");
        }

        if (energyProfileDTO.getDeviceId() == null) {
            errors.add("EnergyProfileDTO DeviceId must not be null.");
        }

        if (energyProfileDTO.getMeasurementId() == null) {
            errors.add("EnergyProfileDTO MeasurementId must not be null.");
        }

        if (energyProfileDTO.getAggregationGranularity() == null) {
            errors.add("EnergyProfileDTO AggregationGranularity must not be null.");
        }

        if (energyProfileDTO.getPredictionGranularity() == null) {
            errors.add("EnergyProfileDTO PredictionGranularity must not be null.");
        }

        if (energyProfileDTO.getEnergyType() == null) {
            errors.add("EnergyProfileDTO EnergyType must not be null.");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(EnergyProfileValidator.class.getSimpleName(), errors);
        }
    }

    public static void validateEnergyProfileParams(
            PredictionType predictionType,
            PredictionGranularity predictionGranularity,
            UUID dataCenterID,
            TopologyComponentType componentType,
            UUID componentID,
            LocalDateTime startTime) {

        List<String> errors = new ArrayList<>();

        if (predictionType == null) {
            errors.add("Prediction type is null");
        }

        if (predictionGranularity == null) {
            errors.add("Prediction granularity is null");
        }

        if (dataCenterID == null) {
            errors.add("Data center ID is null");
        }

        if (componentType == null) {
            errors.add("Component is null");
        }

        if (componentID == null) {
            errors.add("Component is null");
        }

        if (startTime == null) {
            errors.add("Start time is be null");
        }

        if (!errors.isEmpty()) {
            LOGGER.error(errors);
            throw new IncorrectParameterException(EnergyProfileValidator.class.getSimpleName(), errors);
        }
    }
}
