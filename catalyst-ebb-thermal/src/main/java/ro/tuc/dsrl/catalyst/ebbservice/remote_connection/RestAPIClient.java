package ro.tuc.dsrl.catalyst.ebbservice.remote_connection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ro.tuc.dsrl.catalyst.ebbservice.remote_connection.url.DbApiUrl;
import ro.tuc.dsrl.catalyst.ebbservice.remote_connection.url.PredictionApiUrl;

import java.util.Map;

@Component
public final class RestAPIClient {

    private final RestAPIConfig restAPIConfig;

    @Autowired
    public RestAPIClient(RestAPIConfig restAPIConfig) {
        this.restAPIConfig = restAPIConfig;
    }

    // DB methods
    public <T> T getObject(DbApiUrl url, Class<T> responseType, Map<String, ?> urlVariables) {
        return RestTemplateWrapper.getObject(restAPIConfig.getDbApiHostUrl() + url.value(), responseType, urlVariables);
    }

    public <T> T postObject(DbApiUrl url, Object o, Class<T> clazz) {
        return RestTemplateWrapper.postObject(restAPIConfig.getDbApiHostUrl() + url.value(), o, clazz);
    }

    public <T> T postObject(
            DbApiUrl url, Object o, Class<T> clazz, Map<String, ?> urlVariables) {
        return RestTemplateWrapper.postObject(
                restAPIConfig.getDbApiHostUrl() + url.value(), o, clazz, urlVariables);
    }

    // Prediction methods
    public <T> T postObject(PredictionApiUrl url, Object o, ParameterizedTypeReference<T> reference) {
        return RestTemplateWrapper.postObject(restAPIConfig.getPythonHostUrl() + url.value(), o, reference);
    }

    public <T> T getObject(PredictionApiUrl url, Class<T> responseType, Map<String, ?> urlVariables) {
        return RestTemplateWrapper.getObject(url.value(), responseType, urlVariables);
    }

    public <T> T postObject(
            PredictionApiUrl url, Object o, ParameterizedTypeReference<T> reference, Map<String, ?> urlVariables) {

        return RestTemplateWrapper.postObject(
                restAPIConfig.getPythonHostUrl() + url.value(), o, reference, urlVariables);
    }

    private static class RestTemplateWrapper {

        public static <T> T postObject(
                String url, Object o, ParameterizedTypeReference<T> reference, Map<String, ?> urlVariables) {

            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            return rt.exchange(url, HttpMethod.POST, new HttpEntity<>(o), reference, urlVariables).getBody();
        }

        public static <T> T postObject(String url, Object o, Class<T> clazz) {
            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            return rt.postForObject(url, o, clazz);
        }

        public static <T> T postObject(String url, Object o, Class<T> clazz, Map<String, ?> urlVariables) {
            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            return rt.postForObject(url, o, clazz, urlVariables);
        }

        public static <T> T postObject(String url, Object o, ParameterizedTypeReference<T> reference) {
            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            return rt.exchange(url, HttpMethod.POST, new HttpEntity<>(o), reference).getBody();
        }

        public static <T> T getObject(String url, Class<T> responseType, Map<String, ?> urlVariables) {
            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            return rt.getForObject(url, responseType, urlVariables);
        }

        public static <T> ResponseEntity<T> getObject(String url, HttpHeaders headers,
                                                      Class<T> responseType, Map<String, ?> urlVariables) {
            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            HttpEntity httpEntity = new HttpEntity(headers);
            return rt.exchange(url, HttpMethod.GET, httpEntity, responseType, urlVariables);
        }

        public static <T> T getObject(String url, Class<T> responseType) {
            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            return rt.getForObject(url, responseType);
        }

        public static <T> ResponseEntity<T> getObject(String url, HttpHeaders headers,
                                                      Class<T> responseType) {
            RestTemplate rt = new RestTemplate();
            rt.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            rt.getMessageConverters().add(new StringHttpMessageConverter());
            HttpEntity httpEntity = new HttpEntity(headers);
            return rt.exchange(url, HttpMethod.GET, httpEntity, responseType);
        }
    }
}
