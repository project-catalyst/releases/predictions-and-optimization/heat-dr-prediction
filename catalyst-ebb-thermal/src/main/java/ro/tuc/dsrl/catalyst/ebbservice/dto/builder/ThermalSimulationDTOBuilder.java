package ro.tuc.dsrl.catalyst.ebbservice.dto.builder;


import ro.tuc.dsrl.catalyst.model.dto.EnergySampleDTO;
import ro.tuc.dsrl.catalyst.model.dto.ProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.ThermalProfileDTO;
import ro.tuc.dsrl.catalyst.model.dto.ThermalSimulationDTO;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ThermalSimulationDTOBuilder {

    private ThermalSimulationDTOBuilder() {}

    public static ThermalSimulationDTO fromTemperatureData(
            LocalDateTime startTime,
            ProfileDTO temperaturePredictedValues) {

        List<EnergySampleDTO> energySampleDTOs = new ArrayList<>();

        /*
        :)))))))))
        100 seconds at 10 second granularity
        10 values for each sensor
        14 sensor values
        first 14 values -> temperature values at each sensor at time 0
        ...

         * inlet probe 1 rack 1
         * inlet probe 2 rack 1
         * inlet probe 3 rack 1
         * inlet probe 4 rack 1
         * inlet probe 1 rack 2
         * inlet probe 2 rack 2
         * inlet probe 3 rack 2
         * inlet probe 4 rack 2
         * outlet probe 1 rack 1
         * outlet probe 1 rack 2
         * outlet probe 1 room
         * outlet probe 2 rack 1
         * outlet probe 2 rack 2
         * outlet probe 2 room
         *
         * */
        List<String> sensors = new ArrayList<String>();
        sensors.add("inlet probe 1 rack 1");
        sensors.add("inlet probe 2 rack 1");
        sensors.add("inlet probe 3 rack 1");
        sensors.add("inlet probe 4 rack 1");
        sensors.add("inlet probe 1 rack 2");
        sensors.add("inlet probe 2 rack 2");
        sensors.add("inlet probe 3 rack 2");
        sensors.add("inlet probe 4 rack 2");
        sensors.add("outlet probe 1 rack 1");
        sensors.add("outlet probe 1 rack 2");
        sensors.add("outlet probe 1 room");
        sensors.add("outlet probe 2 rack 1");
        sensors.add("outlet probe 2 rack 2");
        sensors.add("outlet probe 2 room");

       int granularity = 10;

       ThermalSimulationDTO  thermalSimulationDTO= new ThermalSimulationDTO();
       Map<String, ThermalProfileDTO> thermalSimulations = new HashMap<String, ThermalProfileDTO>();
       for(String s:sensors){
           thermalSimulations.put(s,new ThermalProfileDTO());
       }
        int index = 0;
        int size = temperaturePredictedValues.getValues().size();
        int columns = sensors.size();
        int rows = size / columns;
        //iterate matrix on columns - each column corresponds to a sensor measured over 100 seconds with 10 second granularity
        for(int j = 0; j < columns; j++){
            String sensorName = sensors.get(j);
            LocalDateTime sampleTime = startTime;
            ThermalProfileDTO tempValues = new ThermalProfileDTO();
            for(int i = 0; i< rows; i++)
            {
                int ind = i * rows + j;
                double val = temperaturePredictedValues.getValues().get(ind);
                EnergySampleDTO aux =new EnergySampleDTO(val, sampleTime);
                sampleTime = sampleTime.plusSeconds(granularity);
                tempValues.getTemperature().add(aux);
            }
            thermalSimulations.put(sensorName,tempValues);
        }
        thermalSimulationDTO.setThermalSimulations(thermalSimulations);
        return thermalSimulationDTO;
    }
}
